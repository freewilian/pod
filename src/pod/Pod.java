/*
 * Implementações  para a Disciplina de Pesquisa e Ordenação de Dados
 * Universidade Federal de Santa Maria
 */
package pod;
import java.util.Random;

/**
 *
 * @author Joel da Silva
 */
public class Pod {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("=================================");
        //define a quantidade de elementos do vetor
        int tamanho = 100;  
        
        //cria o vetor 
        int[] v = new int[tamanho];
       
        //insere valores aleatórios no vetor criado
        insereValores(v);
        
        //imprime valores;
         System.out.println("===== Vetor Original =====");
        imprimeValores(v);
        
        //ordena o vetor com insertion sort
        int n = v.length;
        long tempoInicial = System.currentTimeMillis();
        InsertionSort(v,n);
         
        long tempoFinal = System.currentTimeMillis();
     // System.out.printf("Tempo de Ordenação %.3f ms%n", (tempoFinal - tempoInicial) / 1000d);  
        
        //imprime valores ordenados;
        System.out.println("===== Vetor Ordenado =====");
        imprimeValores(v);
        
        System.out.println("=================================");
        
     
    }
    
    //recebe um vetor e preenche com valores aleatórios
    static void insereValores(int[] v){
         // instancia um objeto da classe Random 
       // Random gerador = new Random(1000); //gera sempre os mesmos valores
         Random gerador = new Random(); //gera valores diferentes a cada vez
         int n = v.length;
        //insere valores randômicos no vetor (valores entre 0 e 99)
        for (int i = 0; i < n; i++){
          v[i] = gerador.nextInt(100);       
        }
    }
    
    
    //recebe um vetor e imprime os elementos
    static void imprimeValores(int[] v) {
        int n = v.length;
         //imprime valores do vetor
        for (int i = 0; i < n; i++){
           // System.out.println(elementos[i]);
           System.out.printf("[%d] = %2d\n", i, v[i]);
        }
    }
    
/*
    Ordena um vetor de comprimento n com uma implementação do Insertion Sort
    @param v vetor a ser ordenado
    @param n tamanho do vetor
*/
    static void InsertionSort(int[] v, int n)
    {      
        int comparacoes = 0;
        int realocacoes =0;
        for (int i=1; i<n; ++i)
            
        {
            int key = v[i];
            int j = i-1;    
             //  /* Teste de Mesa - Debug
             System.out.printf("==== Passo(%d) i=%d, j=%d, key=%d ==== \n",i,i,j,key);
             System.out.println("Situação Inicial");
             imprimeValores(v);
           //  */
            while (j>=0 && v[j] > key)
            {             
                v[j + 1] = v[j];                
                j = j-1;
                realocacoes ++;
            }    
            v[j+1] = key;   
            System.out.println("Situação Final");
            comparacoes++;
          //  /* Teste de Mesa - Debug
             System.out.printf("==== Comparações=%d, Realocações=%d ==== \n",comparacoes,realocacoes);
             realocacoes =0;
             imprimeValores(v);
           // */
        }
    
    }  
    
    
    //===================== 
         /*   
          //Implmementação utilizando For par ao Insertion Sort
     
            for ( j = i - 1; j >= 0 && v[j] > key; j--)
            {
                    v[j + 1] = v[j];
                    v[j] = key;
            }                       
          */     
    //=====================
    

    
    

} //fim principal
